package com.zulfiqar.example.picoyplacapredictor;

import android.widget.DatePicker;
import android.widget.TimePicker;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> activityRule =
            new ActivityTestRule<>(HomeActivity.class);

    public void testNumberPlate(String numberPlate) {
        onView(withId(R.id.et_license_plate))
                .perform(typeText(numberPlate), closeSoftKeyboard());
    }

    public void testSetDate(int date, int month, int year) {
        onView(withId(R.id.et_date)).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(date, month, year));
        onView(withId(android.R.id.button1)).perform(click());
    }


    private void testTime(int hour, int minutes) {

        onView(withId(R.id.et_time)).perform(click());
        onView(withClassName(Matchers.equalTo(TimePicker.class.getName()))).perform(PickerActions.setTime(hour, minutes));
        onView(withId(android.R.id.button1)).perform(click());
    }



    //test Cases

    @Test
    public void firstTestCase() {
        testNumberPlate("");
        testSetDate(27, 9, 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());
    }
    @Test
    public void secondTestCase() {
        testNumberPlate("YJ-078A");
        testSetDate(27, 9, 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());
    }
    @Test
    public void thirdTestCase() {
        testNumberPlate("YJ_07");
        testSetDate(27, 9, 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());

    }
    //Right Test Case
    @Test
    public void fourthTestCase() {
        testNumberPlate("XZ 077");
        testSetDate(26, 9, 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void fifthTestCase() {
        testNumberPlate("XZA.077");
        testSetDate(27, 9, 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());

    }

    @Test
    public void sixthTestCase() {
        testNumberPlate("XZA 07A");
        testSetDate(26, 9, 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());

    }
    //
    @Test
    public void seventhTestCase() {
        testNumberPlate("XZA 071");
        testSetDate(23,9 , 2019);
        testTime(7, 45);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void EgithTestCase() {
        testNumberPlate("PCU 072");
        testSetDate(23,9 , 2019);
        testTime(11, 38);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void NinthTestCase() {
        testNumberPlate("PCU 071");
        testSetDate(23,9 , 2019);
        testTime(16, 38);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void TenthTestCase() {
        testNumberPlate("PCU 074");
        testSetDate(23,9 , 2019);
        testTime(16, 38);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void EleventhTestCase() {
        testNumberPlate("PCU 071");
        testSetDate(24,9 , 2019);
        testTime(16, 38);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void TwelveTestCase() {
        testNumberPlate("PCU 077");
        testSetDate(29,9 , 2019);
        testTime(7, 38);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void ThirteenTestCase() {
        testNumberPlate("PCU 075");
        testSetDate(25,9 , 2019);
        testTime(7, 38);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void FourteenTestCase() {
        testNumberPlate("PCU 0750");
        testSetDate(27,9 , 2019);
        testTime(17, 50);
        onView(withId(R.id.btn_check)).perform(click());
        onView(withId(R.id.btn_ok)).perform(click());
    }
    @Test
    public void FifteenTestCase() {
        testNumberPlate("PCU 0750");
        onView(withId(R.id.btn_check)).perform(click());

    }
    @Test
    public void Sixteen() {
        testNumberPlate("PCU 0750");
        testSetDate(27,8 , 2019);
        onView(withId(R.id.btn_check)).perform(click());

    }
}