package com.zulfiqar.example.picoyplacapredictor.beanClass;

public class PicoPlacaBean {
    private String day;
    private String time;
    private char lastChar;

    public PicoPlacaBean( char lastChar,String day, String time) {
        this.day = day;
        this.time = time;
        this.lastChar = lastChar;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public char getLastChar() {
        return lastChar;
    }

    public void setLastChar(char lastChar) {
        this.lastChar = lastChar;
    }
}
