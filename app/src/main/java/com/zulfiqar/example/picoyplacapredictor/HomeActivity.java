package com.zulfiqar.example.picoyplacapredictor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.zulfiqar.example.picoyplacapredictor.beanClass.PicoPlacaBean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@SuppressLint("SetTextI18n")
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etLicensePlateNumber;
    private EditText etDate;
    private EditText etTime;
    private Calendar calendar;
    private char lastDigitOfNumberPlate;
    private CardView cdMessage;
    private TextView tvMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        //Initializing Variables
        etLicensePlateNumber = findViewById(R.id.et_license_plate);
        etDate = findViewById(R.id.et_date);
        etTime = findViewById(R.id.et_time);
        Button btnCheck = findViewById(R.id.btn_check);
        Button btnClear = findViewById(R.id.btn_clear);
        Button btnOk = findViewById(R.id.btn_ok);
        cdMessage = findViewById(R.id.cv_message);
        tvMessage = findViewById(R.id.tv_message);
        calendar = Calendar.getInstance();
        //Click Listener of Button
        btnCheck.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        //Methods to open picker of date and time
        datePicker();
        timePicker();

    }

    private void datePicker() {
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        };

        etDate.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            new DatePickerDialog(HomeActivity.this, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        etDate.setText(sdf.format(calendar.getTime()));
    }

    private void timePicker() {
        etTime.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(HomeActivity.this, (timePicker, selectedHour, selectedMinute) -> etTime.setText(selectedHour + ":" + selectedMinute), hour, minute, true);//Yes 24 hour time
            mTimePicker.show();

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_check:
              /*  If its Monday the number plate lat digit 1&2 car must not on road same as other days
                on tuesday 3&4,wed 5&6 ,thur 7&8 ,friday 9&0*/
                String userEnteredPlateNumber = etLicensePlateNumber.getText().toString();
                String userEnteredDate = etDate.getText().toString();
                String userEnteredTime = etTime.getText().toString();
                if (validateInputs(userEnteredPlateNumber, userEnteredDate, userEnteredTime)) {
                    lastDigitOfNumberPlate = userEnteredPlateNumber.charAt(userEnteredPlateNumber.length() - 1);
                    String dayOfTheWeek = (String) DateFormat.format("EEEE", convertStringToDate(userEnteredDate));
                    PicoPlacaBean picoPlacaBean = new PicoPlacaBean(lastDigitOfNumberPlate, dayOfTheWeek, userEnteredTime);
                    checkPicoPlacaPredictor(picoPlacaBean);
                }

                break;
            case R.id.btn_clear:
            case R.id.btn_ok:
                etLicensePlateNumber.getText().clear();
                etDate.getText().clear();
                etTime.getText().clear();
                cdMessage.setVisibility(View.GONE);
                break;
        }
    }

    private void checkPicoPlacaPredictor(PicoPlacaBean picoPlacaObject) {
        char lastChar = picoPlacaObject.getLastChar();
        String day = picoPlacaObject.getDay().toUpperCase();
        String time = picoPlacaObject.getTime();

        switch (day) {
            case "MONDAY":
                if (lastChar == '1' || lastChar == '2') {
                    compareTime(time, day);
                } else {
                    carAllowedMessage();
                }
                break;
            case "TUESDAY":
                if (lastChar == '3' || lastChar == '4') {
                    compareTime(time, day);
                } else {
                    carAllowedMessage();
                }
                break;
            case "WEDNESDAY":
                if (lastChar == '5' || lastChar == '6') {
                    compareTime(time, day);
                } else {
                    carAllowedMessage();
                }
                break;
            case "THURSDAY":
                if (lastChar == '7' || lastChar == '8') {
                    compareTime(time, day);
                }else{
                    carAllowedMessage();
                }
                break;
            case "FRIDAY":
                if (lastChar == '9' || lastChar == '0') {
                    compareTime(time, day);
                } else {
                    carAllowedMessage();
                }
                break;

            case "SATURDAY":
            case "SUNDAY":
                carAllowedMessage();
                break;
        }
    }

    private Date convertStringToDate(String userEnteredDate) {
        Date date = null;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).parse(userEnteredDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private boolean validateInputs(String userEnteredPlateNumber, String userEnteredDate, String userEnteredTime) {
        if (!userEnteredPlateNumber.equalsIgnoreCase("")) {
            if (validateNumberPlate(userEnteredPlateNumber)) {
                lastDigitOfNumberPlate = userEnteredPlateNumber.charAt(userEnteredPlateNumber.length() - 1);
                if (isLastCharacterDigit(lastDigitOfNumberPlate)) {
                    if (!userEnteredDate.equalsIgnoreCase("")) {
                        if (!userEnteredTime.equalsIgnoreCase("")) {
                            return true;
                        } else {
                            Toast.makeText(HomeActivity.this, "Please Enter Time", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(HomeActivity.this, "Please Enter Date", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HomeActivity.this, "Last Character must be digit", Toast.LENGTH_SHORT).show();

                }
            }
        } else {
            Toast.makeText(HomeActivity.this, "Please Enter License Plate Number", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private boolean isLastCharacterDigit(char lastDigitOfNumberPlate) {
        return lastDigitOfNumberPlate >= 48 && lastDigitOfNumberPlate <= 57;

    }

    public boolean validateNumberPlate(String userEnteredPlateNumber) {

        if (!userEnteredPlateNumber.matches("[a-zA-Z0-9 -]+")) {
            Toast.makeText(this, "Number Plate must contain alphanumeric", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!userEnteredPlateNumber.matches("^.*[0-9].*$")) {
            Toast.makeText(this, "Number Plate must contain numbers", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!userEnteredPlateNumber.matches("^.*[a-zA-Z].*$")) {
            Toast.makeText(this, "Number Plate must contain letters", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void compareTime(String userEnteredTime, String dayOfTheWeek) {
        int morningStartTime = 700;//7:00(hour*100+minutes)
        int morningEndTime = 930;//9:30
        int afternoonStartTime = 1600;//16:00(16*100+00)
        int eveningEndTime = 1900;//19:30
        String message;
        int userSelectedTime = Integer.parseInt(userEnteredTime.split(":")[0]) * 100 + Integer.parseInt(userEnteredTime.split(":")[1]);
        if (userSelectedTime >= morningStartTime && userSelectedTime <= morningEndTime) {
            message = "Car Cannot be on the road in the city on " + dayOfTheWeek + " between 7:00 to 9:30";
            carNotAllowedMessage(message);
        } else if (userSelectedTime >= afternoonStartTime && userSelectedTime <= eveningEndTime) {
            message = "Car Cannot be on the road in the city on " + dayOfTheWeek + " between 16:00 to 19:30";
            carNotAllowedMessage(message);
        } else {
            carAllowedMessage();
        }
    }

    public void carNotAllowedMessage(String message) {
        cdMessage.setVisibility(View.VISIBLE);
        tvMessage.setText(message);
    }

    public void carAllowedMessage() {
        String message = "Car can be on the road in the city.There is no restrictions.";
        cdMessage.setVisibility(View.VISIBLE);
        tvMessage.setText(message);
    }
}
